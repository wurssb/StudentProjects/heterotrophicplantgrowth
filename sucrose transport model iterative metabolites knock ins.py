import cobra
import pandas
import escher
import json
pandas.options.display.max_rows=400
import cobra.test
import libsbml
from cobra import Model, Reaction, Metabolite
from cobra import flux_analysis
#below are FVA components

from cobra.flux_analysis import flux_variability_analysis
from cobra.util.solver import linear_reaction_coefficients

model = cobra.io.read_sbml_model("arabidopsis model.xml")
model.objective = "R35"

#rna degradation
model.reactions.R401.bounds=(4.6e-7,1000)

#protein degradation
model.reactions.R402.bounds=(0.0000677,1000)

#atp mainenance
model.reactions.R400.bounds=(0,0)

##to switch between photosynthetic growth and growth on starch, the code below
##has to be commented out. In addition, the same operation must be performed in
##the 'for i in range(0,413):' loop. For growth on heterotrophic growth substrate
##the reaction 'R345' has to have bounds closed to (0,0), so that there is no
##starch uptake.
##IGNORE the warnings

##photosynthetic growth

##model.reactions.R344.bounds=(0,15)#photon flux
##model.reactions.R345.bounds=(-1000,-0.000158)
###model.reactions.R345.bounds=(0,0)
##model.reactions.R222.bounds=(-1000,1000) #CO2 uptake
##cyclicConstraints=model.problem.Constraint(.1*model.reactions.R57.flux_expression - model.reactions.R58.flux_expression, lb=0, ub=0)
##model.add_cons_vars(cyclicConstraints) #cyclic electron flow
##photorespirationConstraints=model.problem.Constraint(0.25*model.reactions.R197.flux_expression - model.reactions.R196.flux_expression, lb=0, ub=0)
##model.add_cons_vars(photorespirationConstraints) #photorespiration

##starch growth

model.reactions.R345.bounds = (0,0.000158) #starch import
model.reactions.R222.bounds = (-1000,1000) #CO2 uptake
model.reactions.R344.bounds=(0,0) #photon flux

modd = model.optimize()

print modd.objective_value

##for i in range(len(model.metabolites)):
for i in range(0,413):
    if '_me' in model.metabolites[i].id:
        model = cobra.io.read_sbml_model("arabidopsis model.xml")
        ##model = cobra.io.read_sbml_model("arabidopsis model photorespiratory bypass.xml")
        model.objective = "R35"

        ##for i in model.reactions:
        ##    if i.reversibility == True:
        ##        i.bounds = (-10000,10000)
        ##    else:
        ##        i.bounds = (0,10000)

        #rna degradation
        model.reactions.R401.bounds=(4.6e-7,1000)
        #model.reactions.R401.bounds=(0,0)

        #protein degradation
        model.reactions.R402.bounds=(0.0000677,1000)
        #model.reactions.R402.bounds=(0,1000)

        #model.reactions.R35.bounds=(0,.011)

        ###photosynthetic growth
##        model.reactions.R344.bounds=(0,15)#photon flux
##        model.reactions.R345.bounds=(-1000,-0.000158)
##        model.reactions.R222.bounds=(-1000,1000) #CO2 uptake
##        cyclicConstraints=model.problem.Constraint(.1*model.reactions.R57.flux_expression - model.reactions.R58.flux_expression, lb=0, ub=0)
##        model.add_cons_vars(cyclicConstraints) #cyclic electron flow
##        photorespirationConstraints=model.problem.Constraint(0.25*model.reactions.R197.flux_expression - model.reactions.R196.flux_expression, lb=0, ub=0)
##        model.add_cons_vars(photorespirationConstraints) #photorespiration

        #starch growth
        model.reactions.R345.bounds = (0,0.000158) #starch import
        model.reactions.R222.bounds = (-1000,1000) #CO2 uptake
        model.reactions.R344.bounds=(0,0) #photon flux
        
        h_me=model.metabolites.get_by_id('h_me')
        h_r=model.metabolites.get_by_id('h_r')
        mtblt=model.metabolites[i]
        stringg = 'EXCH_%s' % mtblt.id
        stringg=Reaction(stringg)
        stringg.name='temporary reaction'
        stringg.lower_bound = -1
        stringg.upper_bound = 1
        stringg.add_metabolites({ mtblt:-1.0 , h_me:-1.0 })
        model.add_reaction(stringg)
        moddi = model.optimize()
        if moddi.objective_value > modd.objective_value + 0.001 :
            print model.metabolites[i], moddi.objective_value, model.reactions.get_by_id(stringg.id).flux
##            for i in model.reactions:
##                print i.id, i.flux
        model.reactions.get_by_id(stringg.id).delete()
